
// A non playable character in the game

PlayPath = new Mongo.Collection('play_path');

PlayPath.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  inNpCharacterCode:{
    type:String,
    optional: true
  },
  inPlayerResponseCode:{
    type:String,
    optional: true
  },
  inScenarioCode:{
    type:String,
    optional: true
  },
  outNpCharacterCode:{
    type:String,
    optional: true
  },
  outNPCharacterRequestCode:{
    type:String,
    optional: true
  },
  outScenarioCode:{
    type:String,
    optional: true
  }

}));